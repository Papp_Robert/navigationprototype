package com.navigation.rpapp.navigationprototype.model

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import java.util.*

@RealmClass
open class FormState(@PrimaryKey var id : String,  var parameters : RealmList<String>, var isDeleted : Boolean, var level : Int) : RealmObject() {
    constructor() : this(UUID.randomUUID().toString(), RealmList(), false, 0)

    override fun toString(): String {
        return "${id} parameters: $parameters"
    }
}
