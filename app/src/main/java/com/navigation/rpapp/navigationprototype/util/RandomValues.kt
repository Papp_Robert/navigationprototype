package com.navigation.rpapp.navigationprototype.util

class RandomValues {


    companion object {
        private val filmTitles = arrayOf(
            "Da Vinci Code, The",
            "Daddy Day Camp",
            "Daddy Day Care",
            "Daffy Duck and the Dinosaur",
            "Dallas Buyers Club",
            "Dan in Real Life",
            "Dance of the Dead",
            "Dancer In The Dark",
            "Dances with Wolves",
            "Dangerous Beauty",
            "Daredevil",
            "Darjeeling Limited, The",
            "Dark City",
            "Dark Knight, The",
            "Dark Knight Rises, The",
            "Dark Passage",
            "Dark Side of the Moon, The (1990)",
            "Das Boot",
            "Das Leben der Anderen",
            "From Dusk till Dawn",
            "From Hell",
            "From Hare to Eternity",
            "From Here to Eternity",
            "From Russia with Love",
            "Frost/Nixon",
            "Frozen (2010)",
            "Frozen (2013)",
            "Fuck",
            "Fugitive, The",
            "Full Metal Jacket",
            "Funny Farm",
            "Funny Girl",
            "Furry Vengeance",
            "Fury (1936)",
            "Fury (2014)",
            "Talented Mr. Ripley, The",
            "Tall Tale",
            "Talladega Nights: The Ballad of Ricky Bobby",
            "Tamara (2005)",
            "Tangled",
            "Tangled: Before Ever After",
            "Tao of Steve, The",
            "Targets",
            "Tarzan (1999)",
            "Tarzan II",
            "Taxi",
            "Taxi Driver",
            "Teachers",
            "Team America: World Police",
            "Tears of the Black Tiger",
            "Ted",
            "Ted 2",
            "Teen Titans Go! To the Movies",
            "Teenage Mutant Ninja Turtles (1990)",
            "Teenage Mutant Ninja Turtles II: The Secret of the Ooze",
            "Teenage Mutant Ninja Turtles III",
            "Ten Commandments, The (1956)",
            "Tenacious D in The Pick of Destiny",
            "Terminal, The",
            "Terminator, The"
        )

        fun GenerateNItemRandomValues(n: Int): ArrayList<String> {
            val randomValues: ArrayList<String> = arrayListOf()

            for (i in 1..n) {
                val random = (0 until filmTitles.size).random()
                randomValues.add(filmTitles[random])
            }
            return randomValues
        }
    }


}