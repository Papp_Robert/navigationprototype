package com.navigation.rpapp.navigationprototype

import android.app.Application
import io.realm.Realm
import io.realm.RealmConfiguration
import com.navigation.rpapp.navigationprototype.model.repository.FormStateRepository


class App : Application() {

    companion object {
        var realmInstance : Realm? = null
    }

    override fun onCreate() {
        super.onCreate()

        Realm.init(this)
        // Config without a migration block
        val config1 = RealmConfiguration.Builder()
            .name("realm.db")
            .schemaVersion(1)
            .build()

        realmInstance = Realm.getInstance(config1)

        FormStateRepository().clearDb()
    }
}