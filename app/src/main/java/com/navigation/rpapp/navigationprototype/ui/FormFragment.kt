package com.navigation.rpapp.navigationprototype.ui

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.navigation.rpapp.navigationprototype.R
import com.navigation.rpapp.navigationprototype.data.HomeViewModel
import com.navigation.rpapp.navigationprototype.model.repository.FormStateRepository
import com.navigation.rpapp.navigationprototype.util.ListHelper
import com.navigation.rpapp.navigationprototype.util.RandomValues
import kotlinx.android.synthetic.main.fragment_layout.*

class FormFragment : Fragment() {
    private val formStateRepository = FormStateRepository()

    companion object {
        var Level = 0
    }

    private var viewModel: HomeViewModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //create VM
        activity?.let {
            viewModel = ViewModelProviders.of(it).get(HomeViewModel::class.java)
        }

        setListView()
    }

    private fun setListView() {

        val formState = formStateRepository.getFormStateByLevel(FormFragment.Level)
        var values: ArrayList<String>

        if (formState == null) {
            values = RandomValues.GenerateNItemRandomValues(6)
            viewModel?.parameters?.postValue(values)

        } else {
            values = ListHelper.RealmListToArrayList(formState.parameters)
        }


        val adapter = ArrayAdapter<String>(
            activity?.applicationContext!!,
            android.R.layout.simple_list_item_1, android.R.id.text1, values
        )

        listViewWithRandomElements.adapter = adapter
    }
}