package com.navigation.rpapp.navigationprototype.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.navigation.rpapp.navigationprototype.R
import com.navigation.rpapp.navigationprototype.data.HomeViewModel
import com.navigation.rpapp.navigationprototype.databinding.ActivityHomeBinding
import com.navigation.rpapp.navigationprototype.model.FormState
import com.navigation.rpapp.navigationprototype.model.repository.FormStateRepository
import com.navigation.rpapp.navigationprototype.util.ListHelper
import io.realm.Realm
import java.util.*

class HomeActivity : AppCompatActivity() {

    private var viewModel: HomeViewModel? = null
    private val formStateRepository = FormStateRepository()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityHomeBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_home
        )

        viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)

        binding.viewmodel = viewModel

        supportFragmentManager.beginTransaction().add(
            R.id.formContainer,
            FormFragment()
        ).commit()

        AddNewFragment()

        AddFormStateToRepository()

        CallBackFunction()
    }

    private fun AddFormStateToRepository() {
        viewModel?.parameters?.observe(this, Observer {
            val formState = FormState()

            formState.isDeleted = false
            formState.level = HomeViewModel.Level
            formState.id = UUID.randomUUID().toString()
            formState.parameters = ListHelper.ArrayListToRealmList(it!!)


            formStateRepository.add(formState)
        })
    }

    private fun AddNewFragment() {
        viewModel?.addNewFragmentButtonClicked?.observe(this, Observer {
            addFragment()
        })
    }

    private fun CallBackFunction() {
        viewModel?.backButtonPressed?.observe(this, Observer {
            onBackPressed()
        })
    }

    private fun addFragment() {
        FormFragment.Level++

        val fragmentTransaction = supportFragmentManager.beginTransaction()

        fragmentTransaction.replace(R.id.formContainer, FormFragment(), "top")
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    override fun onDestroy() {
        super.onDestroy()

        Realm.getDefaultInstance().close()
    }

    override fun onBackPressed() {
        FormFragment.Level--

        super.onBackPressed()
    }
}
