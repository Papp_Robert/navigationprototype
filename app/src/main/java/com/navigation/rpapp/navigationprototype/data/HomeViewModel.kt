package com.navigation.rpapp.navigationprototype.data

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import java.util.*

class HomeViewModel : ViewModel() {

    companion object {
        var Level = 0
    }

    var addNewFragmentButtonClicked = MutableLiveData<Boolean>()

    var backButtonPressed = MutableLiveData<Boolean>()

    var parameters = MutableLiveData<ArrayList<String>>()

    fun back() {
        backButtonPressed.postValue(true)
    }

    fun addNewFragment() {
        Level++
        addNewFragmentButtonClicked.postValue(true)
    }
}