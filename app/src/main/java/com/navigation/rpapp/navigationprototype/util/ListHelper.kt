package com.navigation.rpapp.navigationprototype.util

import io.realm.RealmList

class ListHelper {
    companion object {
        fun ArrayListToRealmList(list: ArrayList<String>): RealmList<String> {
            val realmList = RealmList<String>()
            realmList.addAll(list)

            return realmList
        }

        fun RealmListToArrayList(realmList: RealmList<String>): ArrayList<String> {
            val list = arrayListOf<String>()
            list.addAll(realmList)

            return list
        }
    }
}