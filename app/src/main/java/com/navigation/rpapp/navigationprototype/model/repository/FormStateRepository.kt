package com.navigation.rpapp.navigationprototype.model.repository

import com.navigation.rpapp.navigationprototype.App
import com.navigation.rpapp.navigationprototype.model.FormState

class FormStateRepository {

    fun add(formState: FormState) {
        val realm = App.realmInstance!!

        realm.beginTransaction()
        realm.copyToRealmOrUpdate(formState)
        realm.commitTransaction()
    }

    fun getFormStateByLevel(level: Int): FormState? {
        val realm = App.realmInstance!!

        realm.beginTransaction()
        val formState = realm.where(FormState::class.java).and().equalTo("level", level).findFirst()
        realm.commitTransaction()

        return formState
    }

    fun clearDb(){
        val realm = App.realmInstance!!

        realm.beginTransaction()
        realm.where(FormState::class.java).and().findAll().deleteAllFromRealm()
        realm.commitTransaction()
    }
}